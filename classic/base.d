﻿module classic.base;
import helper;
import core.stdc.stdlib;
import std.random, std.stdio, std.string;

abstract class game {	
	/* Globals */
	string[] value = ["Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"];
	string[] suit = ["Clubs", "Diamonds", "Hearts", "Spades"];
	@property uint totalCards() { return (value.length-1) * (suit.length-1); }

	stack deck;
	stack discard;
	stack[] player;
	uint minPlayers = 2;
	uint maxPlayers = 4;

	// Used to know when time for game to exit.
	bool exit;

	this() {
		/* Init the deck and discard */ 
		{
			deck = new stack;
			discard = new stack;
			for (uint i = 0; i < (totalCards); i++) deck.insertBottom(i);
			shuffle(false);
		}

		/* Init the players */ 
		{
			writeln("How many people are going to play? (", minPlayers ,"-", maxPlayers, ")");			
			uint playerCount = toNumber(readln.chomp);			
			while (playerCount < minPlayers || playerCount > maxPlayers) {
				writeln("Invalid input. Please enter a number between ", minPlayers ," and ", maxPlayers, ".");
				playerCount = toNumber(readln.chomp);
			}

			system("@cls||clear");

			writeln("How many cards should each player start with? (1-", (totalCards / maxPlayers), ")");
			uint drawCount = toNumber(readln.chomp);			
			while (drawCount < 1 || drawCount > (totalCards / maxPlayers)) {
				if (drawCount < 1) writeln("Error: Must draw at least 1 card!");
				else writeln("Error: Not enough cards in deck to draw [", drawCount, "] per player!");
				drawCount = toNumber(readln.chomp);
			}
			
			player = new stack[playerCount];
			for (uint i = 0; i < player.length; i++) player[i] = new stack;
						
			foreach(stack p; player) p.draw_fromTop(deck, 1, drawCount);
		}

		/* Start actual game */
		{
			system("@cls||clear");
			playGame();
		}
	}

	void shuffle(bool mixDiscard) {
		if (mixDiscard) {
			while (discard.count > 0) {
				deck.draw_fromTop(discard, deck.bottomIndex);
			}
		}
		
		randomShuffle(deck.card);
	}

	void draw(uint index, uint amount, bool mixDiscardOnEmpty = true) {
		if (deck.count == 0) shuffle(mixDiscardOnEmpty);
		if (deck.count == 0) return;

		uint cardsLeft = player[index].draw_fromTop(deck, player[index].bottomIndex, amount);
		if (cardsLeft > 0) draw(index, amount, mixDiscardOnEmpty);
	}

	void discard_deck() {
		if (deck.count == 0) return;
		deck.discard_toBottom(discard, deck.topIndex);
	}
	
	bool discard_player(uint index, uint slot) {
		if (index >= player[index].count) {
			writeln("Error, card does not exist in that slot!");
			return false;
		}
		
		player[index].discard_toBottom(discard, slot);
		return true;
	}

	abstract void playGame();
	abstract void getInput(uint index, string input);
}

class stack {
	/* Actual card array. */
	uint[] card;
	
	/* Misc property values. */
	@property uint topIndex() { return 0; }
	@property uint bottomIndex() { return card.length-1; }
	@property uint topValue() { return card[0]; }
	@property uint bottomValue() { return card[card.length-1]; }
	@property uint count() { return card.length; }
	
	/* Card Functions */	
	// Returns true only if draw was successful.
	bool draw_fromIndex(ref stack target, uint targetIndex, uint thisIndex) {
		if (target.count == 0) return false;
		if (targetIndex > target.bottomIndex) targetIndex = target.bottomIndex;
		insertAt(target.card[targetIndex], thisIndex);
		target.removeAt(targetIndex);
		return true;
	}		
	bool draw_fromTop(ref stack target, uint thisIndex) { return draw_fromIndex(target, target.topIndex, thisIndex); }		
	bool draw_fromBottom(ref stack target, uint thisIndex) { return draw_fromIndex(target, target.bottomIndex, thisIndex); }	
	// Return value if card(s) could not be drawn.
	uint draw_fromTop(ref stack target, uint thisIndex, uint amount) {
		if (target.count - card.length >= 0) {
			for (uint i = 0; i < amount; i++) {
				insertAt(target.topValue, thisIndex);
				target.removeTop();
				thisIndex++;
			}
		}
		else {
			uint targetLeft = target.count;
			uint undrawnLeft = (amount - targetLeft);
			for (uint i = 0; i < targetLeft; i++){
				insertAt(target.topValue, thisIndex);
				target.removeTop();
				thisIndex++;
			} return undrawnLeft;
		} return 0;
	}		
	uint draw_fromBottom(ref stack target, uint thisIndex, uint amount) {
		if (target.count - card.length >= 0) {
			for (uint i = 0; i < amount; i++) {
				insertAt(target.bottomValue, thisIndex);
				target.removeBottom();
				thisIndex++;
			}
		}
		else {
			uint targetLeft = target.count;
			uint undrawnLeft = (amount - targetLeft);
			for (uint i = 0; i < targetLeft; i++){
				insertAt(target.bottomValue, thisIndex);
				target.removeBottom();
				thisIndex++;
			} return undrawnLeft;
		} return 0;
	}	
	// Returns true only if discard was successful
	bool discard_toIndex(ref stack target, uint targetIndex, uint thisIndex) {
		if (card.length == 0 || thisIndex > card.length) return false;			
		target.insertAt(card[thisIndex],targetIndex);
		removeAt(thisIndex);
		return true;
	}	
	bool discard_toTop(ref stack target, uint thisIndex) { return discard_toIndex(target, target.topIndex, thisIndex); }	
	bool discard_toBottom(ref stack target, uint thisIndex) { return discard_toIndex(target, target.bottomIndex, thisIndex); }
	
	/* Array Functions */
	// Insert value to beginning of array.
	void insertTop(uint value) { card = [value] ~ card; }
	// Insert value to end of array.
	void insertBottom(uint value) { card ~= [value]; }
	// Insert value at location in array.
	void insertAt(uint value, uint index) {
		if (index == 0) insertTop(value);
		else if (index >= card.length) insertBottom(value);
		else card = card[0..index] ~ [value] ~ card[index..card.length];
	}
	// Remove value from beginning of array.
	void removeTop() { card = card[1..card.length];}
	//  Remove value from end of array.
	void removeBottom() { card = card[0..card.length - 1];}
	// Remove value at location in array.
	void removeAt(uint index) {
		if (index >= card.length) return;			
		if (index == 0) removeTop();
		else if (index == card.length) removeBottom();
		else card = card[0..index] ~ card[index+1..card.length]; 
	}
}