﻿module classic.test;
import classic.base;
import helper;
import core.stdc.stdlib;
import std.stdio, std.string, std.uni;

class testgame : game
{
	override void playGame() {
		/* Intro */
		{
			debug { writeln("Let's play - Test! : [DEBUG MODE]"); }
			else { writeln("Let's play - Test!"); }
			writeln("Press return to begin...");
			readln();
		}
		
		/* Actual game */
		while (!exit) {
			/* Player turn handled here */
			for (uint i = 0; i < player.length; i++) {
				refreshPlayerScreen(i);
				
				getInput(i, commandInput);
				if (player[i].count == 0) {
					writeln("Player ", i+1 , " is out of cards!");
					exit = true;
					break;
				}
			}
		}
		
		/* Exit game */
		{
			writeln("Exiting game... Press return to continue.");
			readln();
			system("exit");
		}
	}
	
	override void getInput(uint index, string input) {
		if (input.length > 0) {	
			if (multiWorded(input)) {
				string[] arg = splitString(input);
				
				if (arg[0] == "/help") {
					if (arg[1] == "draw") writeln("draw (optional)[amount]: Draws the amount of cards. Defaults to 1 if not specified");
					else if (arg[1] == "play") writeln("play [slot]: Plays the card in the slot. Use hand to see available cards.");
					else if (arg[1] == "clear") writeln("clear : Clears the screen of all text.");
					else if (arg[1] == "end") writeln("end : Ends the current players turn.");
					else if (arg[1] == "playercount") writeln("playercount : Lists the amount of players in the game.");
					else if (arg[1] == "discard") writeln("discard (optional)[-l]: Lists information about the discard pile.");
					else if (arg[1] == "exit") writeln("exit : Ends the game and closes the application.");
				}
				else if (arg[0] == "draw") {
					uint count = toNumber(arg[1]);
					draw(index, count, true);
				}
				else if (arg[0] == "play") {
					uint slot = toNumber(arg[1]);
					if (slot >= player[index].count) 
						writeln("Error: Invalid slot. Please enter [0-", player[index].bottomIndex, "]. Use hand to see available cards."); 
					else discard_player(index, slot);
				}
				else if (arg[0] == "discard") {
					if (arg[1] == "-l") {
						if (discard.count == 0) writeln("Discard pile is empty.");
						else {
							foreach(uint c; discard.card) {
								writeln(value[c % (value.length - 1)], " of ", suit[c / (value.length - 1)]);
							}
						}
					}
					else writeln("Error: Invalid args. Optional arg(s) are [-l].");
				}
				else writeln("Error: Invalid arguments. Check /help for available commands.");
			}
			else {
				if (input == "/help") {
					writeln();
					writeln("[Available commands]");
					writeln();
					writeln("draw (optional)[amount]: Draws the amount of cards. Defaults to 1 if not specified");
					writeln("play [slot]: Plays the card in the slot. Use hand to see available cards.");
					writeln("clear : Clears the screen of all text.");
					writeln("end : Ends the current players turn.");
					writeln("playercount : Lists the amount of players in the game.");
					writeln("discard (optional)[-l]: Lists information about the discard pile.");
					writeln("exit : Ends the game and closes the application.");
					writeln();
				}
				else if (input == "draw") draw(index, 1, true);
				else if (input == "play") writeln("Error: Not enough arguments. Check /help play for available commands.");
				else if (input == "clear") refreshPlayerScreen(index);
				else if (input == "end") { return; }
				else if (input == "playercount") writeln("Player Amount: ", player.length);
				else if (input == "discard") {
					if (discard.count == 0) writeln("Discard pile is empty.");
					else writeln(value[discard.card[discard.bottomIndex] % (value.length - 1)], " of ", suit[discard.card[discard.bottomIndex] / (value.length - 1)]);
				}
				else if (input == "exit") { exit  = true; return; }		
				else writeln("Error: Invalid arguments. Check /help for available commands.");
			}
		}
		
		/* If out of cards then exit to win check - Else wait for more input */
		{
			if (player[index].count == 0) return;
			getInput(index, commandInput);
		}
	}
	
	string commandInput() {
		write("Command > ");
		return toLower(trim(readln.chomp));
	}

	void refreshPlayerScreen(uint index) {
		system("@cls||clear");
		writeln("Player ", index+1, "'s turn...");
		writeln("Hand: [", player[index].count, "]");
		for(uint i = 0; i < player[index].count; i++) {
			writeln("[",i,"] ", value[player[index].card[i] % (value.length-1)], " of ", suit[player[index].card[i] / (value.length-1)]);
		}
		writeln();
		writeln("Type '/help' for a list of commands.");
		writeln();
	}
}