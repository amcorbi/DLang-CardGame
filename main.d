module main;
import classic.test;

/********************************\
|**  Written and produced by:  **|
|**        Damien Gibson       **|
|**             and            **|
|**        Zane Theriau        **|
\********************************/

int main() {
	new testgame();
	return 0; 
}