This product is developed by Damien Gibson & Zane Theriau as a free Open-Source library. It follows the BSD 3-Clause License Agreement.

If you have paid to receive this library you should immediately request your money back.

You may find this products source on http://Gitlab.com/ArchaicSoft/DLang-CardGame

Questions or troubleshooting can be requested to:
SpiceyWolf.ArchaicSoft@outlook.com