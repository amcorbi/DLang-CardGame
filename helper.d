﻿module helper;
import std.conv;

bool multiWorded(string input) {
	if (input.length == 0) return false;
	foreach(char c; input) if (c == ' ') return true;	
	return false;
}

uint toNumber(string input) {
	string toConvert;
	foreach (char c; input) if (c == '0' || c == '1' || c == '2' || c == '3' || c == '4' 
		|| c == '5' ||	c == '6' || c == '7' ||	c == '8' || c == '9') toConvert ~= to!string(c);
	if (toConvert == "") return 0;
	return to!uint(toConvert);
}

string trim(string input) {
	uint start, end;
	for (uint i = 0; i < input.length; i++) if (input[i] != ' ') { start = i; break; }
	for (uint i = input.length - 1; i >= 0; i--) if (input[i] != ' ' || '\n') { end = i + 1; break; }
	return trimMiddle(input[start..end]);
}

string trimMiddle(string input) {
	for (uint i = 0; i < input.length; i++) {
		if (input[i] == ' ' && input[i - 1] == ' ') {
			input = input[0..i] ~ input[i+1..input.length];
			i--;
		}
	}
	return input;
}

string[] splitString(string input) {
	if (input.length == 0 || !multiWorded(input)) return [input];
	
	string[] ret; uint loc = 0;
	
	for (uint i = 0; i < input.length; i++) {
		if (input[i] == ' ') {
			ret ~= input[loc..i];
			loc = i + 1;
		}
	}
	return ret ~= input[loc..input.length];
}